#include <stdio.h>
main()
{

    FILE *inputFile;
    FILE *outputFile;
    inputFile = fopen("timestamp.txt", "r");
    outputFile= fopen("output.txt","w");

    //read file into array
       signed long long nr1[3000];
       int i;
       unsigned long long nr2[3000];
       unsigned long long nr3 = 4294967296;  
       unsigned long long nr4[3000]; 

    //read the numbers into array and convert
       for (i = 0; i < 3000; i++) {
          fscanf(inputFile, "%lu", &nr1[i]);
          nr2[i]=(unsigned long)nr1[i]+nr3;
       }
    //calculete differences
       for (i = 0; i < 3000; i++){
          nr4[i]=nr2[i+1]-nr2[i];
       }
    //print to screen the results
       for (i = 0; i <20; i++) {
	  printf("Original number: %lu\t,Anzahl: %i\n",nr1[i],i);
          printf("Converted number: %lu\n", nr2[i]);
          printf("Difference between: %lu\t,%lu\t is: %lu\n\n",nr2[i+1],nr2[i],nr4[i]);
    //write to file the results
	  fprintf(outputFile,"Original number: %lu\t,Anzahl: %i\n",nr1[i],i);
          fprintf(outputFile,"Converted number: %lu\n", nr2[i]);
          fprintf(outputFile,"Difference between: %lu\t,%lu\t is: %lu\n\n",nr2[i+1],nr2[i],nr4[i]);
	}
   fclose(inputFile);
   fclose(outputFile);	
   return(0); 
}
